function fN(firstName){
	console.log(`First Name: ${firstName}`);
}
fN("Jane");

function lN(lastName){
	console.log(`Last Name: ${lastName}`);
}
lN("Doe");

function email(email){
	console.log(`Email: ${email}`);
}
email("JaneDoe@gmail.com");

function mN(mobileNumber){
	console.log(`Mobile Number: ${mobileNumber}`);
}
mN("971-1911-276");